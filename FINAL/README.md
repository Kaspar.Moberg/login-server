# Overview
After analysing the program the main issues I saw was:
* Users stored in a variable in the app.py file
    * Passwords not hashed an stored in plaintext
* Some fields prone to SQL injection attacks
    * Possible to drop tables if correct input
* As long as a valid username is input, one can login without correct password
* The user, once logged in, can decide which user to send a message as
* The exact query used when performing an operation is shown to the user
    * Searching for a message will show the exact SQL query used to find those messages. Making it easier to perform an SQL injection attack
* No way to log out

# Features
* Able to log in as two users
    * Alice
        * Password: password123
    * Bob
        * Password: bananas
* User can send, recieve and search for messages
    * Send
        * User can send a message to the database and specify who the reciever will be. Reciever can either be empty, meaning it will send to all, or a valid user in the database. It will output an error to the user if the message contains illegal symbols, or if the recipient does not exist as a user in the database.
    * Recieve
        * The user can recieve and be shown messages that they either have:
            * Sent themselves
            * Been sent
            * Messages sent to all
        * Recieved messages refresh every 5 seconds.
    * Search
        * Searches for a message containing the string input. Messages that contain the input string will be marked in red and scrolled up to. If the user searches for '*' or nothing, no messages will be marked.

* Able to log out requiring a new log in the next time the user wants to use the 
program. 

# How to Use
* Make sure the bcrypt package installed is py-bcrypt, not bcrypt
* Start the server with flask run
* Go to localhost:5000
* Log in using one of the two users in the database
    * Log in using incognito if you want to test both users at the same time
* Send a message
    * Type the message
    * Specify a valid recipient in the database
        *  Empty to send to all
* Search for a message
    * Write a string to search for in the database
    * If you want to clear current search, search for * or nothing
* Use log out button to log out of the current user

# Technical
## Whitelist
When it comes to design considerations the most major change I did was whitelist 
the symbols allowed in a username, password, search, and message. This is a very easy way to prevent XSS attacks and SQL injection attacks. By doing this I can ensure that the symbols used in these input fields only contain symbols that are not used in such attacks.
## '?' Placeholder
Sometimes, however, whitelisting will not fix the problem by itself. E.g. the semicolon is allowed to be used in a message, something that could lead to an SQL injection attack. Here I instead use the '?' placeholder and adding the values when executing the 'conn.execute(...)' command. This will make sure the values in the SQL query will be formatted correctly and will 'hopefully' prevent SQL injections from happening.
## Plaintext Users
User IDs and passwords were at the start saved in a dictionary in the app.py file. Storing them in the file itself is a huge security risk especially when they are stored in plaintext. Here I instead added a new table in the 'tiny.db' database called 'auth_data' containing the users UserID, password and token.
## Plaintext Passwords
The passwords of the users were also saved in plaintext inside the user dictionary. To fix this I hash the password and save it in the database table 'auth_data'. With this implementation we are still in danger of rainbow tables attacks guessing our hashes, so I generate a salt for the password and saves that salt within the hash itself. Half of the salt is stored at the beginning of the hash, with the other half at the end.
## From Sender to Recipient
The template made it possible to decide who you send the message from. I change this to rather specify who the user sends a message too. This is a simple input field where the user specifies a recipient. The recipient must be a valid user in the database, and if it is, the message will be saved in the database, aswell as the recipient of the message.
## Recieved messages update
The template had a Show All button which showed all messages in the database as a single output. This would no longer work as the messages now may have a recipient which is not the current user. It was also not pretty. I also wanted the recieved messages to refresh by themselves so the user would not have to manually press the button every time they want to see new messages. To solve all this I first get all messages in the database that has either the current user as sender, or has either all or the current user as recipient. I then create a new "message element" (h3 and p) for each entry found and adds it to output. Finally I run this code every 5 seconds to refresh the messages recieved so that the user is automatically shown new messages sent to them.
## Search
When searching for a message the program runs a query, searching for any message (that the user is either sender or reciever of) and returns it. The javascript code then changes the background color of the h3 element with same ID as the message to red. Lastly it scrolls to the last element.
## Known issues
* Sometimes searching for messages sent during current session will not show up until you refresh the page and search again.
* Scrolling to a searched message does not work
* Request loader not changed to use database
    * Users variable is therefore still in code so it doesnt break (would not let this go to production)

# Questions
1. Threat Model

    Anyone with knowledge with XSS and SQL injections can attack this application. By attacking the application they get access to messages sent aswell as senders and recievers. They may also delete the table if they wish so. 
    * Confidentiality
        * Able to see the contents of all messages sent. Messages are not encrypted
    * Integrity
        * May send messages as other people, weakening the integrity of the messaging system.
    * Availability
        * May send a large amount of requests, slowing down the server or even killing it making it unavailable to users.

2. Attack Vectors

    * SQL Injection
        * Send unwanted queries to the database
    * XSS Attack
        * Send code to the program via inputs to alter the website
    * DDOS
        * Send a large amount of requests to slow down or crash the server.
    * Rainbow Table Attack
        * Using a rainbow table to crack passwords

3. What can we do

    For SQL injections and XSS I have whitelisted a list of symbols that is allowed when inputing in the program. This ensures that only symbols that we want and know to be safe are used when loging in, sending messages, and searching for messages in the database. For SQL injection attacks I also use SQLite '?' placeholder and inject the values later, so they get proper formatting before being executed.

    For the rainbow table attack I have introduced a salt for the passwords which is saved at the beginning and end of the hash to prevent rainbow tables from working.

4. Access control model

    The program uses the Mandatory Access Control model as the users themselves has no control over other users privileges and their privileges are set by the owner.

5. Traceability

    By testing for the different attacks above we can check if our security meets our standards or not. Importantly we need to do extensive logging to ensure that if their is an attack that our testing did not account for that we have acounts of what happened and how it happened so we can fix it asap.

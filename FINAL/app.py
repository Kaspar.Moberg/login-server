from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template
from werkzeug.datastructures import WWWAuthenticate
import flask
from login_form import LoginForm
from json import dumps, loads
from base64 import b64decode
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from markupsafe import escape
import bcrypt
import datetime

tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = b'Z\x1f\x1cHS\x1dw\xea\x16\xa4\xbb\xb0\x9a\xbb\xcd&Z\xb0\x98B\x8d\xdc\xc0"'
# Secret key should ideally be stored in a separate config file and not in the source code

# Add a login manager to the app
import flask_login
from flask_login import login_required, login_user, logout_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


users = {'alice' : {'password' : 'password123', 'token' : 'tiktok'},
         'bob' : {'password' : 'bananas'}
         }

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass

def check_valid_string(text: str, valid_chars: str) -> bool:
    """Checks if the given string contains only characters from the given string of valid characters.

    Args:
        text (str): The string to check.
        valid_chars (str): The string of valid characters.

    Returns:
        bool: True if the string contains only valid characters, False otherwise.
    """
    for c in text.lower():
        if c not in valid_chars.lower():
            return False
    return True

def get_user_from_db(username: str):
    return conn.execute(f"SELECT * FROM auth_data WHERE username = '{username}'").fetchone()

def hash_password(password: str, salt: str = None):
    if salt is None:
        salt = bcrypt.gensalt()
    hashed_pw = bcrypt.hashpw(password, salt)
    return (salt[:len(salt)//2] + hashed_pw + salt[len(salt)//2:])

def compare_password(hashed, pwd):
    salt = hashed[:29//2] + hashed[-29//2:]
    hashed2 = hash_password(pwd, salt)
    
    if hashed == hashed2:
        return True
    else:
        return False

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    user_db = get_user_from_db(user_id)

    if user_db == None:
        return

    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_db[1]
    return user


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')
        u = users.get(uid)
        if u: # and check_password(u.password, passwd):
            return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')
        for uid in users:
            if users[uid].get('token') == auth_params:
                return user_loader(uid)
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path,
                        'index.html', mimetype='text/html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        if not check_valid_string(username, 'abcdefghijklmnopqrstuvwxyz0123456789_'):
            flask.flash('Username can only contain letter [a-z], numbers [0-9] and underscore [_]')
            return render_template('./login.html', form=form)

        password = form.password.data
        if not check_valid_string(password, 'abcdefghijklmnopqrstuvwxyz0123456789_!'):
            flask.flash('Password can only conaint letter [a-z], numbers [0-9], underscore [_] and exclamation mark [!]')
            return render_template('./login.html', form=form)

        u = get_user_from_db(username)
        if compare_password(u[2], password):
            user = user_loader(username)
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index'))
    return render_template('./login.html', form=form)

@app.route('/loginInfo', methods=['GET', 'POST'])
def loginInfo():
    return flask_login.current_user.id

@app.route('/logout', methods=['GET', 'POST'])
def logout():
    return logout_user()

@app.get('/search')
def search():
    query = request.args.get('q') or request.form.get('q') or '*'
    if query == '*' or query == '':
        stmt = f"SELECT * FROM messages WHERE message GLOB ? AND (sender = ? OR (recipient = ? OR recipient = ''))"
    else:
        stmt = f"SELECT * FROM messages WHERE message LIKE ? AND (sender = ? OR (recipient = ? OR recipient = ''))"
        query = f'%{query}%'
    try:
        c = conn.execute(stmt, (query, flask_login.current_user.id, flask_login.current_user.id))
        rows = c.fetchall()
        result = ""
        for row in rows:
            result = f'{result}{dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'ERROR: {e}', 500)

@app.get('/update')
def update():
    stmt = f"SELECT * FROM messages WHERE sender = ? OR (recipient = ? OR recipient = '')"
    try:
        c = conn.execute(stmt, (flask_login.current_user.id, flask_login.current_user.id))
        rows = c.fetchall()
        result = ""
        for row in rows:
            result = f'{result}{dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
def send():
    try: 
        sender = flask_login.current_user.id
        recipient = request.args.get('recipient') or request.form.get('recipient')
        message = request.args.get('message') or request.form.get('message')
        now = datetime.datetime.now()
        sent_at= now.strftime("%A %d %B %Y at %H:%M:%S")
        
        if not message:
            return f'ERROR: missing message'
        
        # Check if recipient was input. Make empty string if not to send to all users
        if not recipient:
            recipient = ""
        
        # Check if the recipient has a valid username. If it is, check if the user exists in the database 
        if not check_valid_string(recipient, 'abcdefghijklmnopqrstuvwxyz0123456789_'):
            return f'ERROR: Recipient can only contain letter [a-z], numbers [0-9] and underscore [_]'
        elif get_user_from_db(recipient.lower()) == None and recipient != "":
            return f'ERROR: Recipient does not exist'
        
        if not check_valid_string(message, 'abcdefghijklmnopqrstuvwxyz0123456789_!.,;:?- '):
            return f'ERROR: Message can only contain letter [a-z], numbers [0-9], underscore [_], exclamation mark [!], comma [,], period [.] and space [ ]'
        
        stmt = f"INSERT INTO messages (sender, recipient, message, sent_at) values (?, ?, ?, ?);"
        conn.execute(stmt, (sender.lower(), recipient.lower(), message, sent_at))

        return message + ";" + sent_at
    except Error as e:
        return f'ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}

@app.get('/coffee/')
def nocoffee():
    abort(418)

@app.route('/coffee/', methods=['POST','PUT'])
def gotcoffee():
    return "Thanks!"

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        recipient TEXT,
        sent_at TEXT,
        message TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS auth_data (
        id integer PRIMARY KEY,
        username TEXT UNIQUE NOT NULL,
        password TEXT NOT NULL,
        token TEXT);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    
    # Insert some test users
    if False:
        c.execute(f'''INSERT INTO auth_data (username, password, token) VALUES ('alice', '{hash_password("password123")}', 'tiktok');''')
        c.execute(f'''INSERT INTO auth_data (username, password) VALUES ('bob', '{hash_password("bananas")}');''')
except Error as e:
    print(e)
    sys.exit(1)
